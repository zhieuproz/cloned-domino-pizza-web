module.exports = {
    parser: '@typescript-eslint/parser', // Specifies the ESLint parser
    extends: [
        'standard',
        'standard-react',
        'plugin:react/recommended', // Uses the recommended rules from @eslint-plugin-react
        'plugin:prettier/recommended',
        'plugin:@typescript-eslint/eslint-recommended',
        'plugin:react-hooks/recommended',
        'prettier'
    ],
    env: {
        node: true
    },
    plugins: ['@typescript-eslint'],
    parserOptions: {
        ecmaVersion: 2022, // Allows for the parsing of modern ECMAScript features
        sourceType: 'module', // Allows for the use of imports
        ecmaFeatures: {
            legacyDecorators: true,
            jsx: true // Allows for the parsing of JSX
        }
    },
    // Place to specify ESLint rules. Can be used to overwrite rules specified from the extended configs
    rules: {
        'space-before-function-paren': 0,
        'react/prop-types': 0,
        'react/jsx-handler-names': 0,
        'react/jsx-fragments': [
            'warn',
            // mode, `syntax` || `element`
            'syntax'
        ],
        // Turn this of since React 17
        'react/react-in-jsx-scope': 'off',
        'react/jsx-no-target-blank': 1,
        'react/no-did-update-set-state': 0,
        'react/no-unused-prop-types': 0,
        'import/export': 0,
        'no-unused-vars': 'off',
        '@typescript-eslint/no-unused-vars': 'warn',
        'no-new': 1,
        'no-unused-expressions': 1,
        // off this, there are cases we want to use the lone block to group a block of code
        'no-lone-blocks': 0,
        'no-use-before-define': 'off',
        'react-hooks/rules-of-hooks': 'error',
        'react-hooks/exhaustive-deps': 'warn',
        'prettier/prettier': ['off', { singleQuote: true }]
    },
    settings: {
        react: {
            version: 'detect' // Tells eslint-plugin-react to automatically detect the version of React to use
        }
    }
}
