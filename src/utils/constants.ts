export const REGEX_EMAIL = /(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)/
export const REGEX_AT_LEAST_1_DIGIT = /(?=.*\d)/
export const REGEX_AT_LEAST_1_UPPERCASE = /(?=.*[A-Z])/
export const REGEX_AT_LEAST_1_LOWERCASE = /(?=.*[a-z])/
export const REGEX_AT_LEAST_1_SPECIAL = /[*@!#%&$.,'/()^~{}]+/
export const REGEX_NUMBER_ONLY = /^[0-9]*$/
export const menuList = [
    {
        name: 'Mã E-Voucher',
        path: '/voucher'
    },
    {
        name: 'Khuyến Mãi',
        path: '/promotion'
    },
    {
        name: 'Thực Đơn',
        path: '/products'
    },
    {
        name: 'Theo Dõi Đơn Hàng',
        path: '/tracking'
    },
    {
        name: 'Danh Sách Cửa Hàng',
        path: '/stores'
    },
    {
        name: 'Blog',
        path: '/blogs'
    }
]

export const USER_ERRORS = {
    FIRST_NAME_EMPTY: 'Vui lòng nhập tên của bạn',
    LAST_NAME_EMPTY: 'Vui lòng nhập họ của bạn',
    PASSWORD_EMPTY: 'Vui lòng nhập mật khẩu của bạn',
    EMAIL_EMPTY: 'Vui lòng nhập email của bạn',
    EMAIL_INVALID: 'Email không hợp lệ'
}

export const PASSWORD_NOT_MATCH_RULES =
    'Mật khẩu ít nhất 8 ký tự gồm chữ hoa, thường, số và ký tự đặc biệt!!!'
