import { CardIcon, UserIcon } from '../../icons'
import { menuList } from '../../utils/constants'
import { Link, NavLink } from 'react-router-dom'
import './header.scss'
import { AppRoutes } from '../../routes/appRoutes'

const Header = () => {
    return (
        <header>
            <div className='logo'>
                <Link to={AppRoutes.Homepage}>
                    <img src='https://dominos.vn/img/logo/domino-horizontal-dark.svg' />
                </Link>
            </div>

            <nav className='list-menu'>
                <ul>
                    {menuList.map((item, index) => {
                        return (
                            <li key={index}>
                                <NavLink to={item.path}>{item.name}</NavLink>
                            </li>
                        )
                    })}
                </ul>
            </nav>

            <div className='setting'>
                <div className='user'>
                    <NavLink to={AppRoutes.Auth}>
                        <UserIcon />
                    </NavLink>
                </div>
                <div className='cart'>
                    <CardIcon />
                </div>
            </div>
        </header>
    )
}

export default Header
