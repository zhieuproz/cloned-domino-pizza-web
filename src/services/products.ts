import { TProduct } from '../types/products'
import { configEnv } from '../utils/configEnv'
import baseService from './baseService'

class ProductServices {
    readonly productUrl = `${configEnv.BASE_API_URL}/products/`

    async getProducts() {
        return baseService.getRequest<TProduct[]>(this.productUrl)
    }
}

const productServices: ProductServices = new ProductServices()
export default productServices
