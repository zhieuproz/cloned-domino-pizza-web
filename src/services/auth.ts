import { TRegisterForm } from '../types/authentication'
import { configEnv } from '../utils/configEnv'
import baseService from './baseService'

class AuthServices {
    readonly registerUrl = `${configEnv.BASE_API_URL}/register/`
    readonly loginUrl = `${configEnv.BASE_API_URL}/login/`

    async register(user: TRegisterForm) {
        return baseService.postRequest<TRegisterForm>(
            this.registerUrl,
            JSON.stringify(user)
        )
    }
}

const authServices: AuthServices = new AuthServices()
export default authServices
