import { EMethodType, TResponseType } from '../types/common'

class BaseService {
    private async createRequest(
        path: string,
        method: EMethodType,
        body?: string
    ) {
        // TODO: add authen here
        // const token = await userService.getAccessToken()
        let mainBody: RequestInit = {
            method,
            headers: {
                // Authorization: `Bearer ${token}`,
                'Content-Type': 'application/json'
            }
        }

        if (method !== EMethodType.GET && method !== EMethodType.HEAD && body) {
            mainBody = { ...mainBody, body }
        }

        return new Request(path, mainBody)
    }

    async getRequest<T>(
        path: string,
        body?: string
    ): Promise<TResponseType<T>> {
        const request = await this.createRequest(path, EMethodType.GET, body)
        const response = await fetch(request)
        const result = await response.json()

        return {
            data: result.data,
            message: result.message
        }
    }

    async postRequest<T>(
        path: string,
        body?: string
    ): Promise<TResponseType<T>> {
        const request = await this.createRequest(path, EMethodType.POST, body)
        const response = await fetch(request)
        const result = await response.json()

        return {
            data: result.data,
            message: result.message
        }
    }
}

const baseService: BaseService = new BaseService()
export default baseService
