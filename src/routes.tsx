import Products from './pages/products/Products'
import Homepage from './pages/home/Home'
import { AppRoutes } from './routes/appRoutes'
import Authentication from './pages/authentication/Authentication'

export const routers = [
    {
        path: AppRoutes.Homepage,
        element: <Homepage />
    },
    {
        path: AppRoutes.Voucher,
        element: null
    },
    {
        path: AppRoutes.Promotion,
        element: null
    },
    {
        path: AppRoutes.Products,
        element: <Products />
    },
    {
        path: AppRoutes.Tracking,
        element: null
    },
    {
        path: AppRoutes.Stores,
        element: null
    },
    {
        path: AppRoutes.Blogs,
        element: null
    },
    {
        path: AppRoutes.Auth,
        element: <Authentication />
    }
]
