export enum EMethodType {
    HEAD = 'HEAD',
    GET = 'GET',
    POST = 'POST',
    PUT = 'PUT',
    PATCH = 'PATCH',
    DELETE = 'DELETE'
}

export type TResponseType<T> = {
    data: T
    message: string
}
