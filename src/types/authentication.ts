export type TLoginForm = {
    email: string
    password: string
}

export type TRegisterForm = {
    firstName: string
    lastName: string
    phoneNumber: string
    email: string
    password: string
    confirmedPassword: string
}

export type TUserRegistrationError = {
    FIRST_NAME_EMPTY: string
    LAST_NAME_EMPTY: string
    EMAIL_ERROR: string
    PASSWORD_ERROR: string
}
