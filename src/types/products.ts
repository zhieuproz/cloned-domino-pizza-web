export type TProduct = {
    id: number
    title: string
    description: string
    price: number
    imageUrl: string
}
