import { REGEX_EMAIL, USER_ERRORS } from '../utils/constants'

class ValidationHelper {
    isNotEmpty = <T>(value?: T) => {
        if (!value) {
            return false
        }
        if (Array.isArray(value)) {
            return value.length > 0
        }
        switch (typeof value) {
            case 'string':
                return (value?.trim?.()?.length ?? 0) > 0
            case 'object':
                return Object.keys(value).length > 0
            default:
                return true
        }
    }

    isEmpty = <T>(value?: T) => !this.isNotEmpty(value)

    validateEmail(email: string) {
        if (this.isEmpty(email)) return USER_ERRORS.EMAIL_EMPTY
        if (REGEX_EMAIL.test(String(email).toLowerCase()) === false)
            return USER_ERRORS.EMAIL_INVALID

        return ''
    }
}

const validationHelper = new ValidationHelper()
export { validationHelper }
