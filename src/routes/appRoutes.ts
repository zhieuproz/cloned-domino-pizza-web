export enum AppRoutes {
    Homepage = '/',
    Voucher = 'voucher',
    Promotion = 'promotion',
    Products = 'products',
    Tracking = 'tracking',
    Stores = 'stores',
    Blogs = 'blogs',
    Auth = 'auth'
}
