import Footer from '../components/Footer/Footer'
import Header from '../components/Header/Header'
import { Outlet } from 'react-router-dom'

const Layout = () => {
    return (
        <div className='page-content-wrapper'>
            <Header />
            <main className='main-content'>
                <Outlet />
            </main>
            <Footer />
        </div>
    )
}

export default Layout
