import {
    TRegisterForm,
    TLoginForm,
    TUserRegistrationError
} from '../../types/authentication'

export const DEFAULT_LOGIN_FORM: TLoginForm = {
    email: '',
    password: ''
}

export const DEFAULT_REGISTER_FORM: TRegisterForm = {
    firstName: '',
    lastName: '',
    phoneNumber: '',
    email: '',
    password: '',
    confirmedPassword: ''
}

export const USERS_REGISTRATION_ERRORS_DEFAULT: TUserRegistrationError = {
    FIRST_NAME_EMPTY: '',
    LAST_NAME_EMPTY: '',
    EMAIL_ERROR: '',
    PASSWORD_ERROR: ''
}
