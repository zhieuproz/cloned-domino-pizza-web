import LoginForm from './LoginForm'
import RegisterForm from './RegisterForm'
import './authentication.scss'
import { useState } from 'react'

type TFormType = 'Login' | 'Register'

const Authentication = () => {
    const [formType, setFormType] = useState<TFormType>('Login')

    return (
        <section className='auth'>
            <nav className='auth-navigation'>
                <span
                    onClick={() => setFormType('Login')}
                    className={formType === 'Login' ? 'active' : ''}
                >
                    Đăng Nhập
                </span>
                <span
                    onClick={() => setFormType('Register')}
                    className={formType === 'Register' ? 'active' : ''}
                >
                    Tạo tài khoản
                </span>
            </nav>

            <div className='auth-section'>
                <div className='auth-background'>
                    <img
                        src='https://dominos.vn/img/bg/modal-signin-signup.png'
                        alt='background authentication image'
                    />
                </div>
                <div className='auth-form'>
                    {formType === 'Login' ? <LoginForm /> : <RegisterForm />}
                </div>
            </div>
        </section>
    )
}

export default Authentication
