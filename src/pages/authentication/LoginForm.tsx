import './authentication.scss'
import { useState, useCallback } from 'react'
import { DEFAULT_LOGIN_FORM } from './constants'
import { TLoginForm } from '../../types/authentication'

enum ELoginForm {
    EMAIL = 'email',
    PASSWORD = 'password'
}

const LoginForm = () => {
    const [loginForm, setLoginForm] = useState<TLoginForm>(DEFAULT_LOGIN_FORM)

    const handleChangeLoginForm = useCallback(
        (value: string, type: ELoginForm) =>
            setLoginForm({ ...loginForm, [type]: value }),
        [loginForm]
    )

    const handleLogin = () => {}

    return (
        <>
            <div className='field'>
                <label>Email</label>
                <input
                    placeholder='Nhập email'
                    value={loginForm.email}
                    onChange={(e) =>
                        handleChangeLoginForm(e.target.value, ELoginForm.EMAIL)
                    }
                />
            </div>

            <div className='field'>
                <label>Mật Khẩu</label>
                <input
                    type='password'
                    placeholder='Nhập mật khẩu'
                    value={loginForm.password}
                    onChange={(e) =>
                        handleChangeLoginForm(
                            e.target.value,
                            ELoginForm.PASSWORD
                        )
                    }
                />
            </div>
            <button onClick={handleLogin}>Đăng nhập</button>
        </>
    )
}

export default LoginForm
