import './authentication.scss'
import { useCallback, useState } from 'react'
import {
    DEFAULT_REGISTER_FORM,
    USERS_REGISTRATION_ERRORS_DEFAULT
} from './constants'
import {
    TRegisterForm,
    TUserRegistrationError
} from '../../types/authentication'
import { validationHelper } from '../../helper/ValidationHelper'
import {
    PASSWORD_NOT_MATCH_RULES,
    REGEX_NUMBER_ONLY,
    USER_ERRORS
} from '../../utils/constants'
import { usePasswordValidation } from '../../hooks/usePasswordValidation'
import authServices from '../../services/auth'

enum ERegisterForm {
    firstName = 'firstName',
    lastName = 'lastName',
    phoneNumber = 'phoneNumber',
    email = 'email',
    password = 'password',
    confirmedPassword = 'confirmedPassword'
}

const RegisterForm = () => {
    const [registerForm, setRegisterForm] = useState<TRegisterForm>(
        DEFAULT_REGISTER_FORM
    )

    const [error, setError] = useState<TUserRegistrationError>(
        USERS_REGISTRATION_ERRORS_DEFAULT
    )

    const [showErrors, setShowErrors] = useState<boolean>(false)

    const { allPassed } = usePasswordValidation({
        password: registerForm.password,
        confirmedPassword: registerForm.confirmedPassword
    })

    const validateErrors = useCallback(() => {
        const errors = {
            FIRST_NAME_EMPTY: validationHelper.isEmpty(registerForm.firstName)
                ? USER_ERRORS.FIRST_NAME_EMPTY
                : '',
            LAST_NAME_EMPTY: validationHelper.isEmpty(registerForm.lastName)
                ? USER_ERRORS.LAST_NAME_EMPTY
                : '',
            EMAIL_ERROR: validationHelper.validateEmail(registerForm.email),
            PASSWORD_ERROR: !allPassed ? PASSWORD_NOT_MATCH_RULES : ''
        }

        setError(errors)
        return errors
    }, [registerForm, allPassed])

    const handleChangeRegisterForm = (value: string, type: ERegisterForm) => {
        if (type === ERegisterForm.phoneNumber) {
            if (REGEX_NUMBER_ONLY.test(value)) {
                setRegisterForm({
                    ...registerForm,
                    [ERegisterForm.phoneNumber]: value
                })
            }
        } else {
            setRegisterForm({ ...registerForm, [type]: value })
        }
    }

    const handleRegister = async () => {
        const errors = validateErrors()
        const isError = !!Object.values(errors).some((item) => item)
        if (isError) {
            if (!showErrors) {
                setShowErrors(true)
            }
            return
        }

        const registerResult = await authServices.register(registerForm)

        console.log('registerResult', registerResult)

        // TODO: handle API errors here!!
        // TODO: redirect after register successfully
    }

    return (
        <>
            <div className='field'>
                <label>Tên của bạn</label>
                <input
                    placeholder='Nhập tên của bạn'
                    value={registerForm.firstName}
                    onChange={(e) =>
                        handleChangeRegisterForm(
                            e.target.value,
                            ERegisterForm.firstName
                        )
                    }
                />
                {showErrors && error.FIRST_NAME_EMPTY && (
                    <span className='text-danger'>
                        {error.FIRST_NAME_EMPTY}
                    </span>
                )}
            </div>

            <div className='field'>
                <label>Họ của bạn</label>
                <input
                    placeholder='Nhập họ của bạn'
                    value={registerForm.lastName}
                    onChange={(e) =>
                        handleChangeRegisterForm(
                            e.target.value,
                            ERegisterForm.lastName
                        )
                    }
                />
                {showErrors && error.LAST_NAME_EMPTY && (
                    <span className='text-danger'>{error.LAST_NAME_EMPTY}</span>
                )}
            </div>

            <div className='field'>
                <label>Số điện thoại</label>
                <input
                    placeholder='Nhập số điện thoại của bạn'
                    value={registerForm.phoneNumber || ''}
                    onChange={(e) =>
                        handleChangeRegisterForm(
                            e.target.value,
                            ERegisterForm.phoneNumber
                        )
                    }
                />
            </div>

            <div className='field'>
                <label>Email</label>
                <input
                    placeholder='Nhập email của bạn'
                    value={registerForm.email}
                    onChange={(e) =>
                        handleChangeRegisterForm(
                            e.target.value,
                            ERegisterForm.email
                        )
                    }
                />
                {showErrors && error.EMAIL_ERROR && (
                    <span className='text-danger'>{error.EMAIL_ERROR}</span>
                )}
            </div>

            <div className='field'>
                <label>Mật khẩu</label>
                <input
                    type='password'
                    placeholder='Nhập mật khẩu của bạn'
                    value={registerForm.password || ''}
                    onChange={(e) =>
                        handleChangeRegisterForm(
                            e.target.value,
                            ERegisterForm.password
                        )
                    }
                />
                {showErrors && error.PASSWORD_ERROR && (
                    <span className='text-danger'>{error.PASSWORD_ERROR}</span>
                )}
            </div>

            <div className='field'>
                <label>Xác nhận mật khẩu</label>
                <input
                    type='password'
                    placeholder='Xác nhận mật khẩu của bạn'
                    value={registerForm.confirmedPassword || ''}
                    onChange={(e) =>
                        handleChangeRegisterForm(
                            e.target.value,
                            ERegisterForm.confirmedPassword
                        )
                    }
                />
                {showErrors && error.PASSWORD_ERROR && (
                    <span className='text-danger'>{error.PASSWORD_ERROR}</span>
                )}
            </div>
            <button onClick={handleRegister}>Đăng Ký</button>
        </>
    )
}

export default RegisterForm
