import { useEffect, useState } from 'react'
import { TProduct } from '../../types/products'
import './products.scss'
import productServices from '../../services/products'

const Products = () => {
    const [products, setProducts] = useState<TProduct[]>([])

    const getProducts = async () => {
        const response = await productServices.getProducts()
        if (response.data.length) {
            setProducts(response.data)
        }
    }

    useEffect(() => {
        getProducts()
    }, [])

    return (
        <section className='product-list'>
            {products.map((product, index) => {
                return (
                    <div key={index} className='product'>
                        <img src={product.imageUrl} alt={product.title} />

                        <div className='name'>
                            <a href='#'>{product.title}</a>
                        </div>

                        <div className='price'>{product.price}d</div>
                    </div>
                )
            })}
        </section>
    )
}

export default Products
