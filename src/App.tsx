import {
    Route,
    createBrowserRouter,
    createRoutesFromElements
} from 'react-router-dom'
import { routers } from './routes'
import Layout from './pages/Layout'

const WithRouter = createBrowserRouter(
    createRoutesFromElements(
        <Route path='/' element={<Layout />}>
            {routers.map((item, index) => {
                return (
                    <Route
                        key={index}
                        path={item.path}
                        element={item.element}
                    />
                )
            })}
        </Route>
    )
)

export default WithRouter
