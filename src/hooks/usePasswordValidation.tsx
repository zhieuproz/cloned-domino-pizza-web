import { useCallback } from 'react'

import { validationHelper } from '../helper/ValidationHelper'
import {
    REGEX_AT_LEAST_1_DIGIT,
    REGEX_AT_LEAST_1_LOWERCASE,
    REGEX_AT_LEAST_1_SPECIAL,
    REGEX_AT_LEAST_1_UPPERCASE
} from '../utils/constants'

type TUsePasswordvalidation = {
    password: string
    confirmedPassword?: string
    checkEmptyOnly?: boolean
}

export const usePasswordValidation = (props: TUsePasswordvalidation) => {
    const { password, confirmedPassword = '', checkEmptyOnly } = props

    const validatePassword = useCallback(() => {
        return {
            at_least_8_char: password.length > 7,
            at_least_1_digit: !!password.match(REGEX_AT_LEAST_1_DIGIT),
            at_least_1_uppercase: !!password.match(REGEX_AT_LEAST_1_UPPERCASE),
            at_least_1_lowercase: !!password.match(REGEX_AT_LEAST_1_LOWERCASE),
            at_least_1_special: !!password.match(REGEX_AT_LEAST_1_SPECIAL),
            match_together: !!(
                password &&
                confirmedPassword &&
                password === confirmedPassword
            )
        }
    }, [password, confirmedPassword])

    let allPassed = false
    if (checkEmptyOnly) {
        allPassed = !validationHelper.isEmpty(password)
    } else {
        allPassed = !!Object.values(validatePassword()).every((item) => item)
    }

    return {
        allRules: validatePassword(),
        allPassed
    }
}
